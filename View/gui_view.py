#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
    View class for GUI with tkinter
"""

__author__ = "Michael Patron"

from tkinter import Label, Menu, Tk, StringVar, Frame, RIGHT, \
    HORIZONTAL, X, BOTTOM, CENTER, Button, Entry, RIDGE, BOTH, \
    LEFT, LabelFrame, Listbox, Scrollbar, Canvas, YES, scrolledtext
from tkinter.messagebox import showerror
from tkinter.filedialog import askopenfilename, asksaveasfile


class View:
    """
    Class View
    """

    def __init__(self, controller):
        """
        View class constructor

            Arguments:
                self: Current View object
                controller: Controller object
        """
        # Add to the view the controller
        self.controller = controller
        # Conserve information about all the frame produce
        # in the format "window" : [Current window ,
        # methode call, higher window, have to delete root window, *file label]
        # *file label only for chose an element from history
        self.info_window = {}
        # Permit knowing if the programme have to execute "Professional" or "Pedagogic mode"
        # by default initialise execution mode in "Professional" mode
        self.current_mode = "Professional"
        # Initialized attribute containing widget for display result
        # None by default
        self.pedagogic_impression = None
        # Autoincrement id for file conserved in file_history attribute
        self.history_id = 0
        # Conserve file id and file path of manipulated files in format
        # "file_name": [file_id, file_path]
        self.file_history = {}
        # Take the name of the current window
        self.current_window = None
        # Take the name of the file selected in listbox
        self.selected_file = None
        # Conserve button for add execution text
        # in pedagogic mode
        self.pedagogic_continue_button = None
        # Dictionary with all font policy use
        self.font = {"Helvetica_10": "Helvetica 10",
                     "Helvetica_10_bold": "Helvetica 10 bold",
                     "Helvetica_12_bold": "Helvetica 12 bold"}

    def get_current_mode(self):
        """
        Return current_mode attribute

            Argument:
                self: Current View object

            return:
                self.current_mode: Current mode executed
        """
        return self.current_mode

    def start_interface(self):
        """
        Initialize root window and wait for uer interaction
            Argument:
                self: Current View object
        """
        # Create the root window
        master_window = Tk()
        # Assign a title to the root window
        master_window.title("Sequence manipulation programs")
        # Assign a height and a width to root window master_window
        master_window.geometry("1900x850")
        # Assign to the current_window attribute the name of the window display
        self.current_window = "master_window"
        # Initialise variable with tkinter widget Menu.
        # Widget Menu take for argument the root window master_window
        barre_menu = Menu(master_window)
        # Initialize a subMenu in the Menu
        program_menu = Menu(barre_menu)
        # Add to the program_menu 4 subMenu names
        # "Burrows Weeler construction", "Burrows Weeler reconstruction"
        # "Huffman compression", "Huffman decompression" and an executable command

        # Command for execute frame_sequence_to_bwt function View function,
        # destroy the actual window for generate the new one
        program_menu.add_command(label="Burrows Wheeler construction",
                                 command=lambda:
                                 (self.info_window[self.current_window][0].destroy(),
                                  self.frame_sequence_to_bwt(master_window)))
        # Command for execute frame_bwt_to_sequence function,
        # destroy the actual window for generate the new one
        program_menu.add_command(label="Burrows Wheeler reconstruction",
                                 command=lambda:
                                 (self.info_window[self.current_window][0].destroy(),
                                  self.frame_bwt_to_sequence(master_window)))
        # Command for execute frame_compression_sequence View function,
        # destroy the actual window for generate the new one
        program_menu.add_command(label="Huffman compression",
                                 command=lambda:
                                 (self.info_window[self.current_window][0].destroy(),
                                  self.frame_compression_sequence(master_window)))
        # Command for execute frame_decompression_sequence View function,
        # destroy the actual window for generate the new one
        program_menu.add_command(label="Huffman decompression",
                                 command=lambda:
                                 (self.info_window[self.current_window][0].destroy(),
                                  self.frame_decompression_sequence(master_window)))
        # Initialize a file_menu subMenu in the Menu
        file_menu = Menu(barre_menu)
        # Add to the file_menu subMenu, menu command name "Load File" and a command for execute
        # choose_file View function
        file_menu.add_command(label="Load File", command=self.choose_file)
        # Initialize a parameter_menu subMenu in the Menu
        parameter_menu = Menu(barre_menu)
        # Initialize a mode_menu subMenu in the parameter_menu subMenu
        mode_menu = Menu(parameter_menu)
        # Add to the mode_menu subMenu, menu command name "Load File" and
        # a command for set current window mode
        # by "Pedagogic" mode. Execute current_window View function
        mode_menu.add_command(label="Pedagogic",
                              command=lambda:
                              self.set_execution_mode("Pedagogic", self.current_window))
        # Add to the mode_menu subMenu, menu command name "Load File" and a
        # command for set current window mode by "Professional" mode.
        # Execute current_window View function
        mode_menu.add_command(label="Professional",
                              command=lambda:
                              self.set_execution_mode("Professional", self.current_window))
        # Add to parameter_menu subMenu, mode_menu subMenu and name it "Execution Mode"
        parameter_menu.add_cascade(label="Execution Mode", menu=mode_menu)
        # Add to barre_menu Menu, file_menu subMenu and name it "File"
        barre_menu.add_cascade(label="File", menu=file_menu)
        # Add to barre_menu Menu, parameter_menu subMenu and name it "Parameters"
        barre_menu.add_cascade(label="Parameters", menu=parameter_menu)
        # Add to barre_menu Menu, program_menu subMenu and name it "Program"
        barre_menu.add_cascade(label="Program", menu=program_menu)
        # Add to barre_menu Menu, quit for destroy master_window
        barre_menu.add_command(label="Quit", command=lambda: master_window.destroy())
        # Add to th root window barre_menu Menu
        master_window.config(menu=barre_menu)
        # Assign to info_window attribute the window, global function, higher window,
        # boolean for know if we have to delete root window
        self.info_window["master_window"] = [master_window, self.start_interface, None, True]
        # Execute historic View function for create history window in the root window
        self.historic(master_window)
        # Execute explanation_frame View function
        self.explanation_frame(master_window)
        # mainloop for maintain to keep running the program
        master_window.mainloop()

    def explanation_frame(self, master_window):
        """
        Create and display a frame in the root window
        at the beginning of the program

            Argument:
                self: Current View object
                master_window: Root window
        """
        # Create a new frame in the root frame
        information_frame = Frame(master_window)
        # Define policy style use in the function
        bold_titre_style = self.font["Helvetica_12_bold"]
        normal_style = self.font["Helvetica_10"]
        bold_subtitle_style = self.font["Helvetica_12_bold"]
        # Assign to the current_window attribute the name of the window in display
        self.current_window = "information_frame"
        # Create a label in information_frame like title of the frame
        # with a text, a font parameter and a relative position
        Label(information_frame,
              text="Sequence manipulation programs",
              font=bold_titre_style).pack()
        # Create a 2 labelFrame in information_frame with the name
        # of algorithms and a font parameter
        burrows_wheeler_labelframe = LabelFrame(information_frame,
                                                text="Burrows Wheeler programs",
                                                fg="black", font=normal_style)
        huffman_labelframe = LabelFrame(information_frame,
                                        text="Huffman programs",
                                        fg="black", font=normal_style)
        # Create a Label in information_frame with text
        Label(information_frame,
              text="\nThis program use Burrows Wheeler "
                   "and Huffman algorithms:", font=normal_style).pack()
        # Create a Label in information_frame with text
        Label(information_frame,
              text="\nFor access to these programs select the corresponding program in barre menu.",
              font=normal_style).pack()
        # Create a Label in information_frame with text
        Label(information_frame, text="Click on program in barre menu and click on the program to use. You "
                                      "will be redirected in corresponding program window", font=normal_style).pack()
        # Create a Label in information_frame with text
        Label(information_frame, text="\n\nDescription of 4 programs", font=bold_subtitle_style).pack()
        # Create a Label in Burrows_Wheeler_labelframe for explain Burrow Wheeler construction program
        Label(burrows_wheeler_labelframe,
              text="°Burrow Wheeler construction\n\nTake DNA sequence and transform it to\nBurrows Wheeler format",
              font=normal_style).pack(side=LEFT, padx=30)
        # Create a Label in Burrows_Wheeler_labelframe for explain Burrow Wheeler reconstruction program
        Label(burrows_wheeler_labelframe,
              text="°Burrow Wheeler reconstruction\n\nTake sequence in Burrows Wheeler format\nand reconstruct DNA"
                   "sequence",
              font=normal_style).pack(
            side=RIGHT, padx=30)
        # Create a Label in huffman_labelframe for explain Huffman compression program
        Label(huffman_labelframe,
              text="°Huffman compression\n\nCompress sequence in file with octet format\nCompatible with DNA or "
                   "BWT sequence format", font=normal_style).pack(side=LEFT, padx=30)
        # Create a Label in huffman_labelframe for Huffman decompression program
        # \n\t at end of the text for create virtual second line for have same
        # architecture and position in the 4 programs descriptions
        Label(huffman_labelframe, text="°Huffman decompression\n\nDecompress sequence contain in file octet format\n\t",
              font=normal_style).pack(side=RIGHT, padx=30)
        # Configure the position of information_frame in the root window
        information_frame.pack(side=LEFT, fill=BOTH, expand=1)
        # Assign to info_window attribute the window, global function, higher window,
        # boolean for know if we have to delete root window
        self.info_window["information_frame"] = [information_frame,
                                                 self.explanation_frame,
                                                 master_window, False]
        # Positioned 2 LabelFrame in information_frame
        burrows_wheeler_labelframe.pack(fill=X, padx=20, ipady=50, pady=10)
        huffman_labelframe.pack(fill=X, padx=20, ipady=50, pady=50)

    def set_execution_mode(self, mode, window_name):
        """
        Function for modify current_mode attribute

            Arguments:
                self: Current View object
                mode: Name of the new mode in string
                window_name: Name of the actual window
        """
        # Set up current_mode attribute with the new mode
        self.current_mode = mode
        # Recover information about the actual window with
        # research of information in info_window attribute
        # by th name of the actual window
        window = self.info_window[window_name]
        # If we have to destroy the root window and the higher window is the root window.
        # window[3] contain boolean for know if we have to destroy root window and
        # verify if window[2] contain root window
        if window[3] is True and isinstance(window[2], Tk):
            # Destroy the root window
            window[2].destroy()
        # Retrieve the current window with window[0] and destroy it
        window[0].destroy()
        # If higher window is not the root window
        if window[2] is not None:
            # Execute the function for create the window if the higher window.
            # window[1] is the skeleton of the function and window[2] arguments
            # for the function
            window[1](window[2])
        else:
            # Execute the function for create the window.
            # window[1] is the function
            window[1]()

    def frame_sequence_to_bwt(self, master_window):
        """
        Create frame for bwt transformation program

            Arguments:
                self: Current View object
                master_window: Root window
        """

        # Create a new frame in the root window
        bwt_frame = Frame(master_window)
        # Configure the position of the frame at left with the capacity
        # to extend the frame on both direction
        bwt_frame.pack(side=LEFT, fill=BOTH, expand=1)
        # Assign to the current_window attribute the name of the window in display
        self.current_window = "bwt_frame"

        # Assign to info_window attribute the window, global function, higher window,
        # boolean for know if we have to delete root window
        self.info_window["bwt_frame"] = [bwt_frame, self.frame_sequence_to_bwt,
                                         master_window, False]

        # Execute interface_generation View function, in argument the current frame, the name of
        # frame to create and the function to execute
        self.interface_generation(bwt_frame, "Burrows Weeler Transformation: construction",
                                  self.controller.launch_bwt_construction)

    def frame_bwt_to_sequence(self, master_window):
        """
        Create frame for reverse bwt program

            Arguments:
                self: Current View object
                master_window: Root window
        """
        # Create a new frame in the root window
        sequence_frame = Frame(master_window)
        # Configure the position of the frame at left with the capacity
        # to extend the frame on both direction
        sequence_frame.pack(side=LEFT, fill=BOTH, expand=1)
        # Assign to the current_window attribute the name of the window in display
        self.current_window = "sequence_frame"

        # Assign to info_window attribute the window, global function, higher window,
        # boolean for know if we have to delete root window
        self.info_window["sequence_frame"] = [sequence_frame,
                                              self.frame_bwt_to_sequence, master_window,
                                              False]

        # Execute interface_generation View function, in argument the current frame, the name of
        # frame to create and the function to execute
        self.interface_generation(sequence_frame, "Burrows Weeler Transformation: Reconstruction",
                                  self.controller.launch_reverse_bwt)

    def frame_compression_sequence(self, master_window):
        """
        Create frame for Huffman compression program

            Arguments:
                self: Current View object
                master_window: Root window
        """
        # Create a new frame in the root window
        compression_frame = Frame(master_window)
        # Configure the position of the frame at left with the capacity
        # to extend the frame on both direction
        compression_frame.pack(side=LEFT, fill=BOTH, expand=1)
        # Assign to the current_window attribute the name of the window in display
        self.current_window = "compression_frame"

        # Assign to info_window attribute the window, global function, higher window,
        # boolean for know if we have to delete root window
        self.info_window["compression_frame"] = [compression_frame, self.frame_compression_sequence,
                                                 master_window, False]

        # Execute interface_generation View function, in argument the current frame, the name of
        # frame to create and the function to execute
        self.interface_generation(compression_frame, "Compression Huffman",
                                  self.controller.launch_compression)

    def frame_decompression_sequence(self, master_window):
        """
        Create frame for Huffman decompression program
            Arguments:
                self: Current View object
                master_window: Root window
        """
        # Create a new frame in the root window
        decompression_frame = Frame(master_window)
        # Configure the position of the frame at left with the capacity
        # to extend the frame on both direction
        decompression_frame.pack(side=LEFT, fill=BOTH, expand=1)
        # Assign to the current_window attribute the name of the window in display
        self.current_window = "decompression_frame"
        # Assign to info_window attribute the window,
        # global function, higher window, boolean for know
        # if we have to delete root window
        self.info_window["decompression_frame"] = [decompression_frame,
                                                   self.frame_decompression_sequence,
                                                   master_window, False]

        # Execute interface_generation View function, in argument the current frame, the name of
        # frame to create and the function to execute
        self.interface_generation(decompression_frame, "Decompression Huffman",
                                  self.controller.launch_decompression,
                                  True)

    def pedagogic_display(self, iterable_item, information, use_counter=False, counter=0):
        """
        Display iterable object in pedagogic Text widget

            Argument:
                self: Current View object
                iterable_item: Object in iterable
                use_counter: Boolean for know if we have to use a counter
                counter: int value for the counter
        """
        # Retrieve scrolledtext widget use for pedagogic mode
        # for display program execution
        impression = self.pedagogic_impression
        # Try except for manage exception
        try:
            # If we don't use the counter
            if not use_counter:
                # If iterable_item is a dictionary
                if isinstance(iterable_item, dict):
                    # Make iterable_items() iterable
                    iterable_item = iter(iterable_item.items())
                # Go to the next iteration of iterable_item and affect the result to key_value_tuple
                # The result is tuple with (key, value)
                key_value_tuple = next(iterable_item)
                # Affect the 2 values of the tuple in 2 variables
                key, value = key_value_tuple[0], key_value_tuple[1]
                # If key is "Tree construction:\n"
                if key == "Tree construction:\n":
                    # Change the effect of pedagogic_continue_button for display tree structure
                    # and the title
                    self.pedagogic_continue_button["command"] = lambda: (
                        impression.insert('end', f"{key}"),
                        value[0](value[1]),
                        impression.insert('end', "\n"),
                        self.pedagogic_display(iterable_item, information),
                    )
                # In other case
                else:
                    # Change the effect of pedagogic_continue_button for insert in
                    # pedagogic_impression key, value
                    self.pedagogic_continue_button["command"] = lambda: (
                        impression.insert('end', f"{key}{value}\n"),
                        self.pedagogic_display(iterable_item, information)
                    )
            # If we don't use counter
            else:
                # Make iterable iterable_item
                iterable_item = iter(iterable_item)
                # Go the next iteration of iterable_item
                next_iterable = next(iterable_item)
                # Increase counter by 1
                counter += 1
                # Change the effect of pedagogic_continue_button for insert in
                # pedagogic_impression the counter and the value of iterable_item
                self.pedagogic_continue_button["command"] = lambda: (
                    impression.insert('end', f"Step {counter}: {next_iterable}\n"),
                    self.pedagogic_display(iterable_item, information, use_counter=True, counter=counter)
                )
        # If we capture StopIteration exception
        except StopIteration:
            # Change the effect of pedagogic_continue_button for do nothing
            self.pedagogic_continue_button["command"] = ""
            # Insert in pedagogic_impression double ligne return
            impression.insert('end', "\n\n")
            # For all element in information dictionary
            for key, value in information.items():
                # if the key is "Program"
                if key == "Program":
                    # Insert in pedagogic_impression value
                    impression.insert('end', f"\n\nEnd {value}")
                else:
                    # Insert in pedagogic_impression key, value
                    impression.insert('end', f"{key} {value}\n")

    def interface_generation(self, frame, title, command, decompression=False):
        """
        Generate widget common in many frame and set their position

            Arguments:
                self: Current View object
                frame: tkinter Frame object
                title: String with the name of the program
                command: Skeleton of function to execute
                decompression: Boolean for know if we are in decompression program frame
        """
        # Define policy style use in the function
        bold_titre_style = self.font["Helvetica_12_bold"]
        normal_style = self.font["Helvetica_10"]
        # Define a control variable for string type
        sequence_var_string = StringVar()
        # Create explanation_label frame in the current frame
        explanation_label = Label(frame, text=title, font=bold_titre_style)
        # Configure the position of the frame in a grid
        explanation_label.grid(row=0, columnspan=3, column=0, sticky='n')
        # Create a label to set in the frame and empty text
        label_to_set = Label(frame, text="", font=normal_style)
        # Configure the position of the label in a grid
        label_to_set.grid(row=3, column=0, columnspan=3, pady=30)

        # Create a black ligne with 3 canvas widget for force the use of 3 columns
        Canvas(frame, width=500, height=5, bg='black').grid(row=5, column=0, sticky="ew", pady=30)
        Canvas(frame, width=500, height=5, bg='black').grid(row=5, column=1, sticky="ew", pady=30)
        Canvas(frame, width=500, height=5, bg='black').grid(row=5, column=2, sticky="ew", pady=30)

        # if we are not in decompression program
        if not decompression:
            # Create a label in the frame in grid
            Label(frame, text="Enter your string:", font=normal_style).grid(row=1, column=0,
                                                                            pady=50, sticky="e")

            # Create an entry in the frame and a control variable
            user_entry = Entry(frame, textvariable=sequence_var_string, font=normal_style, background="white")
            # Configure the position of the entry at row 1, column 1 and padding X of 5
            user_entry.grid(row=1, column=1, pady=50, padx=10, sticky="ew")

            # Create a button in frame with text and command compose of the
            # skeleton function and the entry and the label to set in arguments
            algorithm_call = Button(frame, text="Ok", font=normal_style, command=lambda:
                                    command(user_entry, label_to_set))
            # Configure the position of the button in a grid
            algorithm_call.grid(row=1, column=2, sticky="w")
        # if we are in decompression program
        if decompression:
            # Create a label to file in frame with text
            file_label = Label(frame, text="Any file choose", font=normal_style)
            # Configure the position of the label to file in a grid
            file_label.grid(row=2, column=1, sticky="w", padx=30)
            # Execute create_history_file_choose View function with the
            # frame and the label to set in arguments
            self.create_history_file_choose(frame, file_label)
            # Create button in frame with text and 2 commands
            # for execute choose_file View function with true in
            # argument and show_file_decompression View function with the label to file
            local_choose_file = Button(frame, text="Load file from local", font=normal_style,
                                       command=lambda: (self.choose_file(local=True),
                                                        self.show_file_decompression(file_label)))
            # Configure the position of the button in a grid
            local_choose_file.grid(row=1, column=1, padx=30, pady=20, sticky="e")
            # Create a button in frame with text and command compose of
            # the skeleton function with selected_file attribute and the
            # label to set in arguments
            algorithm_call = Button(frame, text="Ok", font=normal_style,
                                    command=lambda: command(self.selected_file, label_to_set))
            # Configure the position of the button in a grid
            algorithm_call.grid(row=2, column=1, padx=30, sticky="e")
        # if we are in pedagogic mode
        if self.current_mode == "Pedagogic":
            # create a label in frame, with text justify
            pedagogic_label = Label(frame, text="Code show", justify=CENTER, font=normal_style)
            # Configure the position of the label in a grid
            pedagogic_label.grid(row=6, column=0, columnspan=3, sticky="n", pady=10)
            # Create a scrollable Text in frame
            pedagogic_txt = scrolledtext.ScrolledText(frame, width=150, background="white")
            # Set pedagogic View attribute with Text widget
            self.pedagogic_impression = pedagogic_txt
            # Configure the position of the Text in grid
            pedagogic_txt.grid(row=7, columnspan=3, padx=10)
            # Create button in frame with text
            continue_button = Button(frame, text="continue", font=normal_style)
            self.pedagogic_continue_button = continue_button
            # Configure the position of the button in a grid
            continue_button.grid(row=6, column=2, sticky="w")

    def create_history_file_choose(self, frame, file_label):
        """
        Create frame for select element in the history

            Arguments:
                self: Current View object
                frame: tkinter Frame object
                file_label: tkinter label
        """
        # Define policy style use in the function
        normal_style = self.font["Helvetica_10"]
        # Create a labelframe in frame with text
        history_file_choose_label = LabelFrame(frame, text="Choose file from history", font=normal_style)
        # Configure the position of labelframe in a grid
        history_file_choose_label.grid(row=1, column=1, padx=30, pady=20, sticky="w")
        # Assign to info_window attribute the window, global function, higher window,
        # boolean for know if we have to delete root window, file label
        self.info_window["history_file_choose_label"] = [history_file_choose_label,
                                                         self.create_history_file_choose,
                                                         frame, False, file_label]
        # Create a listbox in frame
        choose_list = Listbox(history_file_choose_label)
        # Configure the position of the listbox for fill his frame
        choose_list.pack(fill=BOTH)
        # Bind an event in the listbox with 2
        # commands listbox_choose View function with the event in argument and
        # show_file_decompression View function with the file label in argument
        choose_list.bind("<<ListboxSelect>>",
                         lambda event: (self.listbox_choose(event),
                                        self.show_file_decompression(file_label)))
        # for each element in the file_history attribute
        for key, file_information in self.file_history.items():
            # Insert the file at the end of the list box
            choose_list.insert('end', key)

    def historic(self, master_window):
        """
        Create history frame

            Arguments:
                    self: Current View object
                    master_window: Root window
        """
        # Define policy style use in the function
        normal_style = self.font["Helvetica_10"]
        # Create a new frame in the root window
        history_frame = Frame(master_window, borderwidth=1, relief=RIDGE, bg="white")
        # Assign to info_window attribute the window, global function, higher window,
        # boolean for know if we have to delete root window
        self.info_window["history_frame"] = [history_frame, self.historic, master_window, False]
        # Initialize empty list
        key_list = []
        # For each key in file_history dictionary
        for key in self.file_history.items():
            # Add the key in key_list
            key_list.append(key)
        # Create a label in th frame with text, anchor a font parameters
        Label(history_frame, text="History", anchor=CENTER, font=normal_style, bg='white').pack()
        # Create a canvas for represent black line en define minimum and maximum size for history frame
        Canvas(history_frame, width=250, height=5, bg="black").pack(fill=X)
        # Create button in frame with text, anchor, font parameters and
        # command delete_file View function with key_list in argument
        clear_history = Button(history_frame, text="Clear History",
                               anchor=CENTER, font=normal_style,
                               command=lambda: self.delete_file(key_list))
        # Configure the position of the button at the bottom of the frame
        clear_history.pack(side=BOTTOM, pady=15)

        # for each element in file_history attribute
        for name, file_information in self.file_history.items():
            # Initialize variable with the path of the file
            file = file_information[1]
            # Initialize variable with a string including the id and name of the file
            history_name = "Data " + str(file_information[0]) + ": " + name
            # Create a labelframe in frame with text
            file_container = LabelFrame(history_frame, text=history_name, bg='white')
            # Configure the position of the labelframe with padding Y 5
            file_container.pack(fill=X, padx=30)
            # Create a button in the labelframe with text and command
            # save_file View function with file path in argument
            save_file = Button(file_container, text="save", command=lambda: self.save_file(file))
            # Configure the position of the button at the left with padding X 5
            save_file.pack(side=LEFT, padx=30)
            # Create a button in the label frame with text and command
            # delete_file View function with file path in argument
            delete_file = Button(file_container, text="delete",
                                 command=lambda: self.delete_file(name))
            # Configure the position of the button at the right with padding X 5
            delete_file.pack(side=RIGHT, padx=30)

        # Configure the position of the frame at right with the capacity
        # to extend the frame on both direction and with internal padding of 100px
        history_frame.pack(side=RIGHT, fill=BOTH, expand=False, ipadx=100)

    def history_update(self):
        # Retrieve history frame from info_window attribute
        # info_window["history_frame"][0] is history frame
        history_frame = self.info_window["history_frame"][0]
        # Destroy history frame
        history_frame.destroy()
        # If the current window is "decompression_frame"
        if self.current_window == "decompression_frame":
            # Update decompression_frame obtain from info_window
            self.info_window["decompression_frame"][0].update()
            # Destroy history_file_choose_label frame obtain from info_window
            self.info_window["history_file_choose_label"][0].destroy()
            # Retrieve history_file_choose_label construction function in info_window["history_file_choose_label"][1]
            # and execute it with argument obtain from info_window["history_file_choose_label"][2]
            # and info_window["history_file_choose_label"][4]
            self.info_window["history_file_choose_label"][1](
                self.info_window["history_file_choose_label"][2],
                self.info_window["history_file_choose_label"][4])
        # Execute historic for recreate history frame, retrieve history creation function
        # from info_window["history_frame"][1] affected to creation_methode and the
        # master_window from info_window["history_frame"][2]
        creation_methode = self.info_window["history_frame"][1]
        master_window = self.info_window["history_frame"][2]
        creation_methode(master_window)

    def show_file_decompression(self, label):
        """
        Retrieve the name of the file from th path

            Arguments:
                self: Current View object
                label: tkinter label
        """
        # Initialize variable with the path of the selected file
        name_path = self.selected_file[1]
        # Initialize variable with the split of the path on "/" character
        file_name = name_path.split("/")
        #  Attribute to file_name the name of the file by Taken the
        #  last element in the file_name variable
        file_name = file_name[len(file_name) - 1]
        # Set the text of the label with a string with the name of the file
        label["text"] = f"Current file use: {file_name}"

    def listbox_choose(self, select_event):
        """
        Use event for tkinter listbox for select file from history

            Arguments:
                self: Current View object
                select_event: listbox event
        """
        # Execute curselection listbox function and return the element selected in the listbox
        selection = select_event.widget.curselection()
        # if element is selected
        if selection:
            # retrieve the index of the selected value in the listbox
            index = selection[0]

            # Execute get listbox function and return the file at the
            # corresponding index
            file = select_event.widget.get(index)
            # Set selected_file View attribute with the corresponding file
            # in the file_history View attribute
            self.selected_file = self.file_history[file]

    def choose_file(self, local=False):
        """
        Choose a file from a path

            Arguments:
                self: Current View object
                local: False by default, indicate if the file is in a local path
        """
        # Execute askopenfilename function for produce an interface for select
        # a file and return the file
        file_path = askopenfilename(title="Choose your file",
                                    filetypes=[('text file', '.txt'),
                                               ('all file', '.*')])
        # if the path is not empty
        if file_path != "":
            # if the path is not a local path
            if not local:
                # Execute file_handler_2_history Controller function with
                # file path in argument for add the file in history
                self.controller.file_handler_2_history(file_path)
                # If the path is an internal path
            elif local:
                # Execute local_handler_2_frame Controller function Add the
                # file in the frame view
                self.controller.local_handler_2_frame(file_path)

    def add_history(self, file_information):
        """
        Add file in the history

            Arguments:
                self: Current View object
                file_information: list information about the file
        """
        # Autoincrement id
        self.history_id += 1
        # Add to the file_history View attribute the name of the file in key and id and the
        # path of the file in values
        self.file_history[f"{file_information[0]}_{self.history_id}"] = [self.history_id, file_information[1]]
        # Execute history_update View function for dynamically update the history frame
        self.history_update()

    def delete_file(self, key_file):
        """
        Delete a file in the history

            Arguments:
                self: Current View object
                key_file: name of the file to delete
        """
        # If the key is a string
        if isinstance(key_file, str):
            # Delete from file_history View attribute the corresponding couple key-values
            del self.file_history[key_file]
        # If the key is a list
        if isinstance(key_file, list):
            # For each key list in the key_list variable
            for list_file in key_file:
                # Take the key containing in the list
                key = list_file[0]
                # Delete from file_history View attribute the corresponding couple key-values
                del self.file_history[key]
        # Execute history_update View function for dynamically update the history frame
        self.history_update()

    @staticmethod
    def save_file(file_2_save):
        """
        Save a file in local

            Argument:
                file_2_save: file path
        """
        # Variable with different format allowed for save th file
        files = [('All Files', '*.*'),
                 ('Text Document', '*.txt')]
        # Execute asksaveasfile function in write mode
        file = asksaveasfile(mode='w', filetypes=files, defaultextension=files)
        # If any file is try to be saved
        if file is None:
            # End the function
            return
        # If a file is try to be saved, open the file in read mode
        with open(file_2_save, 'r', encoding='UTF-8') as text_save:
            # Read the content of the file
            text_save = text_save.read()
        # Write in the file to save the content of the file
        file.write(str(text_save))
        file.close()

    def sequence_draw(self, sequence):
        """
        Draw a sequence in a tkinter canvas

            Arguments:
                self: Current View object
                sequence: string sequence to draw
        """
        # Define policy style use in the function
        bold_subtitle_style = self.font["Helvetica_12_bold"]
        # Upper sequence character
        sequence = sequence.upper()
        # Strip undesirable element in the string sequence
        sequence = sequence.strip()
        # Recover the window to draw the sequence in info_window View attribute
        # from the current window
        window = self.info_window[self.current_window][0]
        # Create a frame in current window
        draw_frame = Frame(window)
        # Configure the position of the frame in a grid at the row 5 and column 1, with the column
        # extend in 4 columns and a padding Y 30
        draw_frame.grid(row=4, columnspan=3, pady=10)
        # Create a canvas in the draw_frame frame with width 500, height 110 and an ivory background
        canvas = Canvas(draw_frame, width=500, height=70, bg='ivory')
        # Add to the canvas configuration the possibility to have a scroll bar extend in the canvas
        canvas.configure(scrollregion=canvas.bbox('all'))
        # Create a scrollbar in draw_frame frame with horizontal orientation
        scrollbar = Scrollbar(draw_frame, orient=HORIZONTAL)
        # Dynamically set the scrollbar with the x position in the canvas
        scrollbar.config(command=canvas.xview)
        # Execute the scrollbar function for x axe
        canvas.config(xscrollcommand=scrollbar.set)
        # Configure the position of the scrollbar in the bottom of the frame extend in x axe
        scrollbar.pack(side=BOTTOM, fill=X)
        # Configure the position of the frame in the left of the frame extend in both axes
        canvas.pack(side="left", expand=YES, fill=BOTH)
        # Initialized values x1, y1, x2, y2 for draw ligne in canvas
        ligne_x_1 = 5
        ligne_y_1 = 30
        ligne_x_2 = 20
        ligne_y_2 = 30
        # Initialized values x1, y1, x2, y2 for draw rectangle in canvas
        rectangle_x_1 = 20
        rectangle_y_1 = 20
        rectangle_x_2 = 40
        rectangle_y_2 = 40
        # Initialized values x1, y1, for draw text in canvas
        text_x_1 = 30
        text_y_1 = 29
        # Initialized a counter
        position = 0
        # Define a default color for the text
        color = 'white'
        # for each letter in sequence
        # # if a corresponding letter appears set a personal color
        for character in sequence:
            if character == "A":
                color = "#CB4335"
            elif character == "T":
                color = "#9B59B6"
            elif character == "G":
                color = "#A6ACAF"
            elif character == "C":
                color = "#F1C40F"
            elif character == "N":
                color = "#3498DB"
            # Create a line with define axe position , with a width of 2 and a default black color
            canvas.create_line(ligne_x_1, ligne_y_1, ligne_x_2, ligne_y_2,
                               width=2, fill='black')
            # Create a rectangle with define axe position and color, and with a width of 2
            canvas.create_rectangle(rectangle_x_1, rectangle_y_1,
                                    rectangle_x_2, rectangle_y_2, fill=color, width=2)
            # Create a text int the rectangle with define axe position with the letter in text,
            # black default color and a special font parameters
            canvas.create_text(text_x_1, text_y_1, text=character,
                               fill="black", font=bold_subtitle_style)
            # Create a text just under the rectangle with define axe position,
            # with the position in text and a default black color and
            # special font parameters
            canvas.create_text(text_x_1, text_y_1 + 25, text=str(position),
                               fill="black", font=bold_subtitle_style)
            # Change the Y axes of rectangle, text and ligne to be moved in the right
            ligne_x_1 += 35
            ligne_x_2 += 35
            rectangle_x_1 += 35
            rectangle_x_2 += 35
            text_x_1 += 35
            # Increment the position
            position += 1

        # Define the limit axes of th scrollbar
        canvas["scrollregion"] = (-5, 0, ligne_x_1 + 10, ligne_x_2)

    @staticmethod
    def line_canvas(x1, x2, y1, y2, canvas_window):
        """
        Create a ligne in canvas widget

            Arguments:
                x1: x position for place left end of ligne in canvas
                x2: x position for place right end of ligne in canvas
                y1: y position for place left end of ligne in canvas
                y2: y position for place right end of ligne in canvas
                canvas_window: Canvas frame
        """
        # create ligne in canvas_window with x1, y1, x2, y2 position in canvas in arguments
        canvas_window.create_line(x1, y1, x2, y2, width=2, fill='black')

    @staticmethod
    def rectangle_canvas(x1, x2, y1, y2, canvas_window):
        """
        Create a rectangle in canvas widget

            Arguments:
                x1: x position for place upper left corner of rectangle in canvas
                x2: x position for place lower right corner of rectangle in canvas
                y1: y position for place upper left corner of rectangle in canvas
                y2: y position for place lower right corner of rectangle in canvas
                canvas_window: Canvas frame
        """
        # create rectangle in canvas_window with x1, y1, x2, y2 position in canvas in arguments
        canvas_window.create_rectangle(x1, y1, x2, y2, fill="#A6ACAF", width=2)

    def text_canvas(self, x1, y1, canvas_window, character):
        """
        Create a text in canvas widget

        Arguments:
                self: Current View object
                x1: x position for place letter in canvas
                y1: y position for place letter in canvas
                canvas_window: Canvas frame
                character: Character to draw
        """
        # Define policy style use in the function
        normal_style = self.font["Helvetica_10"]
        # create text in canvas_window with x1, y1, x2, y2 position in canvas in arguments
        canvas_window.create_text(x1, y1, text=character, fill="black", font=normal_style)

    @staticmethod
    def error_message(error_message):
        """
        Generate tkinter showerror messagebox

            Argument:
                error_message: Message to write in the box
        """
        # Initialize the title of the bow by "Error occurred"
        # and give error_message to showerror message
        showerror("Error occurred", error_message)
