#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
    Burrow-Weeler Transformation
"""

__author__ = "Michael Patron"

import re


def sequence_to_bwt(sequence):
    """
    Format sequence in Burrows Weeler sequence

        Argument:
            sequence: string sequence to transform

        Return:
            bwt: sequence after Burrows Weeler transformation
    """
    # If the sequence is a string
    if isinstance(sequence, str):
        # Upper all character of the string
        sequence = sequence.upper()
        # Create a regex pattern
        pattern = "^[ATGCN]*$"
        # Check if the sequence respect the pattern
        checked = bool(re.match(pattern, sequence))
        # If pattern is true
        if checked:
            # Add $ character at the end of the sequence
            sequence += "$"
            # Create list for containing all the possible shift
            # during matrix construction
            shift_list = []
            # Initializes a counter
            counter = 0

            # If the counter < length of the sequence
            while counter < len(sequence):
                # Conservation of the last character
                last_character = sequence[len(sequence) - 1]
                # Take all the sequence minus the last character
                sequence = sequence[:len(sequence) - 1]
                # Creation of the new sequence string
                sequence = last_character + sequence
                # Add of the new sequence string in the list
                shift_list.append(sequence)
                # increment the counter
                counter += 1

            # Initialized a string variable by empty string
            bwt = ""
            # For each element in the sorted list
            for element in sorted(shift_list):
                # Add to the string variable the last character of each element
                bwt += element[len(element) - 1]

            return bwt, sorted(shift_list)
        # If pattern is false
        # Raise an exception with message
        raise Exception("Sequence contain invalid character")
    # If sequence is not a string
    # Raise an exception with message
    raise Exception("sequence must be a string of character")


def bwt_to_sequence(bwt):
    """
    Reforme sequence from Burrows Wheeler format

        Argument:
            bwt: Burrows Wheeler format sequence

        Return:
            sequence: Sequence string
    """
    # Initialized empty list for memorise the step of
    # the construction of matrix
    # Create a regex patt
    pattern = "^[ATGCN$]*"
    # Check if the bwt sequence respect the pattern
    checked = bool(re.match(pattern, bwt))
    # If pattern is true
    if checked:
        # If bwt is a string
        if isinstance(bwt, str):
            # Upper all character of the string
            bwt = bwt.upper()
            # initialized a list variable
            matrix = []
            # For each character in bwt sequence
            for element in bwt:
                # Add character in the list variable
                matrix.append(element)
            # Sort the list
            matrix = sorted(matrix)

            # initialized a variable at 0
            i = 0
            # While i variable < or = length bwt sequence - 2
            while i <= len(bwt) - 2:
                # Initialized variable with 0
                index = 0
                # For each element in bwt sequence
                for element in bwt:
                    # Add for the string in the list at index element
                    matrix[index] = element + matrix[index]
                    # Increment index variable
                    index += 1

                # Sort the list
                matrix = sorted(matrix)
                # Increment i variable
                i += 1

            # For element in list
            for element in matrix:
                # if the last character in the string is $ character
                if element[-1] == "$":
                    # Initialized sequence variable with le string with the $
                    # in last position whiteout take the $
                    sequence = element[:len(element) - 1]
                    return sequence, sorted(matrix)

            # If any string corresponding in the list raise exception with message
            raise Exception("Error occurred any element corresponding to the sequence")
        # If bwt is not a string
        # Raise exception with message
        raise Exception("BWT sequence must be string type")
    # If pattern is false
    # Raise exception with message
    raise Exception("Invalid character on the bwt format")


def file_save(file_name, sequence):
    """
    Save data in local file

        Arguments:
            file_name: Name of the file
            sequence: Sequence to save

        return:
            file_path: path of the file
    """
    # Create the path where sav the file with the name of the file
    file_path = f"Data/{file_name}.txt"
    # Create or open if exist the file of the file path in write mode with encoding format UTF-8
    with open(file_path, "w", encoding='UTF-8') as handler:
        # Write in the file the sequence
        handler.write(sequence)
        return file_path
