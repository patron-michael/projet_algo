#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
    Class for Node
"""

__author__ = "Michael Patron"


class Node:
    """
        Node class
    """

    def __init__(self, frequency: float,  letter: str = "intermediary node"):
        """
        Node class constructor

            Arguments:
                self: Current Node object
                frequency: frequency value for the letter
                letter: string character
        """
        # Initialized attribute, possibility for the node to point
        # a node in right
        self.right = None
        # Initialized attribute, possibility for the node to point
        # a node in left
        self.left = None
        # Initialized Attribute, binary string corresponding to the letter
        self.binary = None
        # If letter argument is a string or if letter is None
        if isinstance(letter, str) or letter is None:
            # Initialized attribute with letter argument
            self.letter = letter
        # If letter is not a string
        else:
            # Raise exception with message
            raise Exception("Value node must be of type string")
        # If frequency argument is a float
        if isinstance(float(frequency), float):
            # Initialized attribute with frequency
            self.weight = frequency
        # If frequency argument is not a float
        else:
            # Raise exception with message
            raise Exception("Frequency must be of type float")

    def get_letter(self):
        """
        Return letter attribute

            Argument:
                self: Current Tree object

            Return:
                letter: letter attribute
        """
        return self.letter

    def get_binary(self):
        """
        Return binary attribute

            Argument:
                self: Current Tree object

            Return:
                binary: binary attribute
        """
        return self.binary

    def get_weight(self):
        """
        Return weight attribute

            Argument:
                self: Current Tree object

            Return:
                  weight: weight attribute
        """
        return self.weight

    def addition_weight(self, weight: float):
        """
        Set weight attribute value by add weight argument to weight attribute value

            Arguments:
                self: Current Tree object
                weight: float value
        """
        # If weight argument is a float
        if isinstance(float(weight), float):
            # Set weight attribute value by add weight argument
            # to weight attribute value
            self.weight = self.weight + weight
        # If weight argument is not a float
        else:
            # Raise exception with message
            raise Exception("Frequency must a of type float")

    def set_binary(self, binary):
        """
        Set binary attribute by binary argument

            Argument:
                self: Current Tree object
                binary: binary string
        """
        self.binary = binary

    def set_right(self, node):
        """
        Set right attribute by node argument

        Argument:
            self: Current Tree object
            node: Node Object
        """
        # If node argument is Node type
        if isinstance(node, Node):
            # Affect node to right attribute
            self.right = node
        # If node argument is not Node type
        else:
            # Raise exception with message
            raise Exception("Parameter must be of type Node object")

    def set_left(self, node):
        """
        Set left attribute by node argument

            Argument:
                self: Current Tree object
                node: Node Object
        """
        # If node argument is Node type
        if isinstance(node, Node):
            # Affect node to left attribute
            self.left = node
        # If node argument is not Node type
        else:
            # Raise exception with message
            raise Exception("Parameter must be of type Node object")

    def encoding_sequence(self, node, letter):
        """
        Read binary tree from root node and return corresponding binary code
        for letter

            Argument:
                self: Current Tree object
                node: Root Node Object from Tree
                letter: Character

            Return:
                encoding: String variable with binary code corresponding to the letter
        """
        # If letter is type str
        if isinstance(letter, str):
            # If corresponding node for the letter is found
            if node.letter == letter:
                # Affect to encoding the binary string corresponding to the node
                encoding = node.binary
                return encoding
            # If the node is note the leaf
            if node.right is not None:
                # Recursive call with newt node en the letter in argument
                encoding = self.encoding_sequence(node.right, letter)
                # If recursive function return encoding
                if encoding:
                    return encoding
            # If the node is note the leaf
            if node.left is not None:
                # Recursive call with newt node en the letter in argument
                encoding = self.encoding_sequence(node.left, letter)
                # If recursive function return encoding
                if encoding:
                    return encoding

    # Adapt from https://stackoverflow.com/questions/34012886/print-binary-tree-level-by-level-in-python
    def display_aux(self):
        """
        Returns list of strings, width, height, and horizontal coordinate of the root

            Argument:
                self: Current Tree object

            Return:
                couples of values
        """
        # If th node is a leaf
        if self.right is None and self.left is None:
            # Affect to line letter and binary attribut of the node in string
            line = '%s : %s' % (self.letter, self.binary)
            # Calcul the width of the representation equal to the length
            # of the line, corresponding to x-axis value
            width = len(line)
            # Affect to height a default value of 1, corresponding to
            # y-axis value
            height = 1
            # Calcule the middle length of the representation for x-axis
            middle = width // 2
            # return a list with line, width, height and middle value
            return [line], width, height, middle

        # If we have only a left node
        if self.right is None:
            # Recursive function for recuperate a list with line, width, height and middle value
            lines, n, p, x = self.left.display_aux()
            # Affect to s letter and binary attribut of the node in string
            s = '%s : %s' % (self.letter, self.binary)
            # calcul the length of u
            u = len(s)
            # Create a line of character with space and _ , plus the s string
            # Take into account the middle and the width of the line
            first_line = (x + 1) * ' ' + (n - x - 1) * '_' + s
            # Create a seconde line with space and /. Take into account the middle
            # and the width of the line plus the length of s string
            second_line = x * ' ' + '/' + (n - x - 1 + u) * ' '
            # Create space between the character for create lines
            shifted_lines = [line + u * ' ' for line in lines]
            # Return list with 2 lines, the space between the lines, new width, new height, new middle
            return [first_line, second_line] + shifted_lines, n + u, p + 2, n + u // 2

        # If we have only a right node
        if self.left is None:
            # Recursive function for recuperate a list with line, width, height and middle value
            lines, n, p, x = self.right.display_aux()
            # Affect to s letter and binary attribut of the node in string
            s = '%s : %s' % (self.letter, self.binary)
            # calcul the length of u
            u = len(s)
            # Create a line of character with _ and space , plus the s string
            # Take into account the middle and the width of the line
            first_line = s + x * '_' + (n - x) * ' '
            # Create a seconde line with space and \. Take into account the middle
            # and the width of the line plus the length of s string
            second_line = (u + x) * ' ' + '\\' + (n - x - 1) * ' '
            # Create space between the character for create lines
            shifted_lines = [u * ' ' + line for line in lines]
            # Return list with 2 lines, the space between the lines, new width, new height, new middle
            return [first_line, second_line] + shifted_lines, n + u, p + 2, u // 2

        # For the two node left and right
        # Recursive function in right node for recuperate a list with line, width, height and middle value
        left, n, p, x = self.left.display_aux()
        # Recursive function in left node for recuperate a list with line, width, height and middle value
        right, m, q, y = self.right.display_aux()
        # Affect to s letter and binary attribut of the node in string
        s = '%s : %s' % (self.letter, self.binary)
        # calcul the length of u
        u = len(s)
        # Create a line of character with space and _ , plus the s string
        # Take into account the middle and the width of the line for right and left
        first_line = (x + 1) * ' ' + (n - x - 1) * '_' + s + y * '_' + (m - y) * ' '
        # Create a seconde line with space and /. Take into account the middle
        # and the width for right and left of the line plus the length of s string
        second_line = x * ' ' + '/' + (n - x - 1 + u + y) * ' ' + '\\' + (m - y - 1) * ' '
        # If height for left is lower than height for right
        if p < q:
            # Add to left string space depending on left width and difference between two height
            left += [n * ' '] * (q - p)
        # If height for right is lower than height for left
        elif q < p:
            # Add to right string space depending on left width and difference between two height
            right += [m * ' '] * (p - q)
        # Affect to zipped_lines iterator on tuple value
        zipped_lines = zip(left, right)
        # Create lines  with information about line create previously
        lines = [first_line, second_line] + [a + u * ' ' + b for a, b in zipped_lines]
        # Return list of lines with maximum width, maximum between two height + 2
        # and the middle of the final representation
        return lines, n + m + u, max(p, q) + 2, n + u // 2
