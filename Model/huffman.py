#!/usr/bin/env python3
# -*- coding: utf-8 -*-

__author__ = "Michael Patron"

from re import match
from Model.binary_tree import Tree


def compression_huffman(bwt):
    """
    Huffman compression program for compress a sequence in
    octet and save it in file

        Argument:
            bwt: Sequence to compress

        Return:
            word: Octal equivalent of bwt sequence
            coding_tab: Dictionary containing the decoding key
            tree: Root node of the binary tree
    """
    # Upper all character in bwt sequence
    bwt = bwt.upper()
    # Create a regex pattern
    pattern = "^[ATGCN$]*"
    # Check if the sequence respect the pattern
    checked = bool(match(pattern, bwt))
    # If pattern is true
    if checked:
        # Initialized empty dictionary
        frequency_nucleotide = {}
        # For each nucleotide in the list
        for nucleotide in ["A", "T", "G", "C", "N", "$"]:
            # Add in the dictionary the letter in dictionary key and the frequency
            # of the letter in bwt sequence in the dictionary value
            frequency_nucleotide[nucleotide] = (bwt.count(nucleotide) / (len(bwt)))

        # Execute Tree Model function with the dictionary in argument
        construction = Tree(frequency_nucleotide)
        # Initialized a variable with the root node of the tree
        tree = construction.tree
        # Initialized empty dictionary
        coding_tab = {}
        # Initialize empty string variable
        encoding = ""
        # For each character in bwt sequence
        for letter in bwt:
            # Execute encoding_sequence node function with root node and letter
            # in arguments for retrieve the corresponding binary code of the letter
            letter_coding = tree.encoding_sequence(tree, letter)
            # Add in dictionary the binary code in dictionary key and
            # the letter in dictionary value
            coding_tab[letter_coding] = letter
            # Add the binary code of the letter in string
            # for have complete binary sequence of the bwt sequence
            encoding += letter_coding

        # Initialize empty string variable
        word = ""
        # Initialize int variable for prepare the cut
        # of the bwt sequence every 7 character
        gap_range = 7
        # Initialize int variable corresponding to start index
        start = 0
        # Initialize int variable corresponding to end index
        # equivalent of the sum between the start index and the gap
        end = start + gap_range
        # Cut the binary string every 7 character
        while start < len(encoding):
            # Add a 1 off protection + 7 character for each octet
            code = "1" + encoding[start:end]
            # Traduce binary code in octet en add it to word variable
            word += chr(int(code, 2))

            # Increment start an end index by the gap
            start += gap_range
            end += gap_range
        return word, coding_tab, tree
    # If pattern is false
    # Raise an exception with message
    raise Exception("Invalid character in sequence to compress")


def recover_information_from_file(file_path):
    """
    Read a file with compressed ADN sequence and decoding key

        Arguments:
            file_path: File path in compressed DNA format

        Returns:
            sequence: Compressed DNA sequence
            decoding_tab: Dictionary with decoding key
    """
    # Initialized int variable for count the number
    # of ligne read in the file
    number_ligne = 0
    # Initialized int variable like index
    i = 0
    # Initialized empty list for containing decoding
    # information from file
    list_decoding = []
    # Initialized empty dictionary for containing
    # decoding key and values
    decoding_tab = {}

    # Read file in UTF-8 encoding
    with open(file_path, "r", encoding='UTF-8') as handler:
        # Read the first ligne of the file containing the information
        # about the number of value composing the decoding dictionary
        nb_value = handler.readline()
        # While all the decoding value are not read
        while number_ligne < int(nb_value):
            # Take the value
            value = handler.readline()
            # Add the value in list without space or undesirable character
            list_decoding.append(value.strip())
            # Increment number of ligne read
            number_ligne += 1
            # Divide the number of ligne by 2 because
            # in dictionary we have number of key =
            # number of value
        number_ligne /= 2
        # Initialized int variable counter
        y = 0
        # While counter < number of ligne read
        while y < number_ligne:
            #  Insert in dictionary the key at index i and the value
            # at index i + 1 present in decoding list
            decoding_tab[str(list_decoding[i])] = str(list_decoding[i + 1])
            # Increment by 2 because of the couple key value
            i += 2
            # Increment the counter
            y += 1
        # read the end of the file corresponding to the sequence
        sequence = handler.read()
    return sequence, decoding_tab


def decompression_huffman(compressed_sequence, decoding_tab):
    """
    Huffman decompression program for decompressed octal compressed sequence

        Arguments:
            compressed_sequence: Octal format compressed sequence
            decoding_tab: Decoding key for corresponding compressed sequence

        Return:
            dna_sequence: Decompressed sequence
    """
    # Initialize empty string variable
    decompression_string = ""
    # For each element in compressed sequence
    for element in compressed_sequence:
        # Traduce octal in equivalent binary code
        binary_code = bin(ord(element))
        # For the traduced we remove the indication of binary (0b)
        # in the string and the 1 of protection
        binary_code = binary_code.replace("0b1", "")

        # Add binary code in string
        decompression_string += binary_code

    # Initialize empty string variable for decoding sequence
    decoding_string = ""
    # Initialize empty string variable for decompressed sequence
    dna_sequence = ""
    # For each binary character in the compressed sequence
    for character in decompression_string:
        # Add to decoding_string variable binary character
        decoding_string += character
        # If decoding_string have a correspondence in
        # decoding_tab dictionary
        if decoding_string in decoding_tab:
            # Traduce the binary code by th equivalent letter in dictionary
            dna_sequence += decoding_tab[decoding_string]
            # Reinitialized decoding_string for containing
            # new binary code
            decoding_string = ""
    return dna_sequence, decompression_string


def compression_save(compressed_sequence, encoding_tab):
    """
    Save a file with compressed DNA sequence and decoding key

        Arguments:
            compressed_sequence: Compressed DNA sequence
            encoding_tab: Decoding dictionary with key information
    """
    # Open a file in write mode wit UFT-8 encoding
    with open("Data/compression.txt", "w", encoding='UTF-8') as handler:
        # Write in file the length of the dictionary *2
        # because od the couple key value
        handler.write(f"{len(encoding_tab) * 2}\n")
        # For each key in dictionary
        for key, value in encoding_tab.items():
            # Write in file key, value of decoding dictionary
            handler.write(f"{key}\n{value}\n")
        # Write the compressed sequence in file
        handler.write(compressed_sequence)


def decompression_save(sequence):
    """
    Write in file decompressed DNA sequence

        Argument:
            sequence: Decompressed DNA sequence
    """
    # Open a file with UTF-8 encoding
    with open("Data/decompression.txt", "w", encoding='UTF-8') as handler:
        # write the sequence in file
        handler.write(sequence)
