#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
    Class for binary tree
"""

__author__ = "Michael Patron"

from Model.node import Node


class Tree:
    """
    Tree class
    """

    def __init__(self, frequency_nucleotide):
        """
        Tree class constructor

            Arguments:
                self: Current Tree object
                frequency_nucleotide: Dictionary with letter in key and frequency of
                                      apparition in value
        """
        # If frequency_nucleotide is a dictionary
        if isinstance(frequency_nucleotide, dict):
            # tree attribute is the result of tree_maker Tree function
            self.tree = self.tree_maker(frequency_nucleotide)
            # node attribute is set at None by default
            self.node = None

    def tree_maker(self, frequency_nucleotide):
        """
        Construct binary tree

            Arguments:
                self: Current Tree object
                frequency_nucleotide: Dictionary with letter in key and frequency of
                                      apparition in value

            return:
                tree: complete tree
        """
        # Initialized a variable with a list
        node_list = []
        # for each element in frequency_nucleotide dictionary
        for nucleotide, frequency in frequency_nucleotide.items():
            # Create a Node object with the frequency of the nucleotide and is name in argument
            nucleotide = Node(frequency, nucleotide)
            # Add the Node in the list
            node_list.append(nucleotide)
        # Sort the list by executing quicksort_tree_nodes Tree functions with
        # the list, the first and last index
        reversed_node_list = self.quicksort_tree_nodes(node_list, 0, len(node_list) - 1)
        # While the length of the list is > 1
        while len(reversed_node_list) > 1:
            # If the length is equal to 2
            if len(reversed_node_list) == 2:
                # Create root Node object with the merge of the value of the
                # two first node in the list and with root name
                root_node = Node((reversed_node_list[0].get_weight() +
                                  reversed_node_list[1].get_weight()),
                                 "root")
                # Set binary Node attribute with empty string
                root_node.set_binary("")
                # Set the right Node attribute with the first Node in the list
                # Node with the smaller value always in right
                root_node.set_right(reversed_node_list[0])
                # Set the left Node attribute with the second Node in the list
                # Node with the second smaller value always in right
                root_node.set_left(reversed_node_list[1])

                # Remove the 2 node used for create the new node from the list
                reversed_node_list.remove(reversed_node_list[0])
                reversed_node_list.remove(reversed_node_list[0])

                # Add the new node in the list
                reversed_node_list.append(root_node)
            # If the length is not equal to 2
            else:
                # Create Node object with the merge of the value of the two first node in the list
                intermediary_node = Node(reversed_node_list[0].get_weight() +
                                         reversed_node_list[1].get_weight())

                # Set the right Node attribute with the first Node in the list
                # Node with the smaller value always in right
                intermediary_node.set_right(reversed_node_list[0])
                # Set the left Node attribute with the second Node in the list
                # Node with the second smaller value always in right
                intermediary_node.set_left(reversed_node_list[1])

                # Remove the 2 node used for create the new node from the list
                reversed_node_list.remove(reversed_node_list[0])
                reversed_node_list.remove(reversed_node_list[0])

                # Add the new node in the list
                reversed_node_list.append(intermediary_node)
                # Sort the list by executing quicksort_tree_nodes Tree function with
                # the list, the first and last index
                reversed_node_list = self.quicksort_tree_nodes(reversed_node_list,
                                                               0, len(reversed_node_list) - 1)

        # Binary tree is the last node remain in the list
        tree = reversed_node_list[0]
        # node Tree attribute is the root node
        self.node = tree
        # Execute tree_binarization Tree function for set binary value at each node of the tree,
        # take root node in argument
        self.tree_binarization(tree)
        return tree

    def tree_binarization(self, node):
        """
        Set binary value for each node in the binary tree
            Arguments:
                self: Current Tree object
                node: root node
        """
        # If right Node attribute is not None
        if node.right is not None:
            # Create new string by add "1" at the binary string
            binary = node.get_binary() + "1"
            # Set the binary Tree attribute with the new value
            node.right.set_binary(binary)
            # Execute recursion tree_binarization Tree function with right Node attribute in
            self.tree_binarization(node.right)
        # If left Node attribute  is not None
        if node.left is not None:
            # Create new string by add "0" at the binary string
            binary = node.get_binary() + "0"
            # Set the binary Tree attribute with the new value
            node.left.set_binary(binary)
            # Execute recursion tree_binarization Tree function with left Node attribute in
            self.tree_binarization(node.left)

    def quicksort_tree_nodes(self, node_list, first, last):
        """
        Sort the nodes in a list by the value attribut of the nodes
            Arguments:
                self: Current Tree object
                node_list: list with nodes
                first: first index in the list choose for sort the list
                last: last index in the list choose for sort the list

            Return:
                node_list: Sort list of node by their wight attribut
        """
        # Define a pivot node issue to the half of the sum of the
        # first and last index in the node list
        pivot = node_list[int((first + last) / 2)]
        # Initialize i variable with first index
        i = first
        # Initialize j variable with last index
        j = last
        # While we have value to sort
        while True:
            # while the weight attribute of the node < weight attribute of the node pivot
            while node_list[i].get_weight() < pivot.get_weight():
                # Increment i variable
                i += 1
            # While the weight attribute of the node > weight attribute of the node pivot
            while node_list[j].get_weight() > pivot.get_weight():
                #  Decrement j variable
                j -= 1
            # If i variable > j variable
            if i > j:
                # Interrupt the loop
                break
            # If i variable < j variable
            if i < j:
                # swap the value at index i, j by value at index j, i
                node_list[i], node_list[j] = node_list[j], node_list[i]
            # Increment i variable
            i += 1
            # Decrement j variable
            j -= 1
        # If first index value < j variable
        if first < j:
            # Execute recursion with node list , first and j in argument
            self.quicksort_tree_nodes(node_list, first, j)
        # If i variable < last index
        if i < last:
            # Execute recursion with node list , i and last in argument
            self.quicksort_tree_nodes(node_list, i, last)
        return node_list
