# Algorithmic project
[![Language](https://img.shields.io/badge/Language-Python-lightgrey?style=for-the-badge&logo=python&logoColor=white)]()
[![Project_status](https://img.shields.io/badge/Project-beta%20version-lightgrey?style=for-the-badge&logo=verizon)]()
[![Project_status](https://img.shields.io/badge/GitLab%20link-Projet%20algo-lightgrey?style=for-the-badge&logo=gitlab)](https://gitlab.com/patron-michael/projet_algo)


Interface program for use Burrows Wheeler and Huffman algorithms adapt for use in biology.

Program available:

| Program                       |                                       Description                                        |
|-------------------------------|:----------------------------------------------------------------------------------------:|
| Burrows Weeler construction   |               Take DNA sequence and transform it to Burrows Wheeler format               |
| Burrows Weeler reconstruction |           Take sequence in Burrows Wheeler format and reconstruct DNA sequence           |
| Huffman compression           | Compress sequence in file with octet format. Compatible with DNA or BWT sequence format  |
|  Huffman decompression        |                     Decompress sequence contain in file octet format                     |

During the execution of the program, records the result of all program in a history and allow to save it in local file. 

# Table of contents
* [Prerequisites](#Prerequisites)
* [How to run the program](#How-to-run-the-program)
* [Technologies use](#Technologies-use)
* [Task lists remaining](#Task-lists-remaining)
* [Authors](#Authors)
* [License](#License)

## Prerequisites <a name="Prerequisites"></a>
Interface made with TK GUI toolkit interface and need compatible version.

For correctly use this program you need to install or update that library:

- regex
```
# Tape in console

pip install regex      #Install regex librairy

    or
    
pip install regex --upgrade     #Upgrade regex library
```

## How to run the program <a name="How-to-run-the-program"></a>
For launch program tape in console:
```
python3 .\main.py
```

## Technologies use <a name="Technologies-use"></a>
Use python 3.9.10 version and compatible with python 3.9.*

## Task remaining lists <a name="Task-lists-remaining"></a>

- [ ] Incomplete task
    - In binary_tree.py
      - [ ] Factorise code in tree_maker function
      
    - gui_view.py
        - [ ] Add for Burrow wheeler programs and Huffman compression the possibility to directly use result from historic

## Authors <a name="Authors"></a>
| Members        | Affectation |
|----------------|-------------|
| patron-michael | Owner       |

## License <a name="License"></a>
Open source project.

