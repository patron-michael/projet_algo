#!/usr/bin/env python3
# -*- coding: utf-8 -*-

__author__ = "Michael Patron"

from Controller.controller import Controller

if __name__ == "__main__":

    # Initialized execution variable with Controller object
    execution = Controller()
    # Execute start_view Controller function for start the program interface
    execution.start_view()
