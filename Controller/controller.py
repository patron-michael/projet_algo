#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
    Controller class for algorithmic_project program
"""

__author__ = "Michael Patron"

from View.gui_view import View
from Model.bwt import sequence_to_bwt, bwt_to_sequence, file_save
from Model.huffman import compression_huffman, decompression_huffman, \
    recover_information_from_file, compression_save, decompression_save


class Controller:
    """
    Controller class
    """

    def __init__(self):
        """
        Controller class constructor

            Argument:
                self: Current Controller object
        """
        # Assign View object to Controller view attribute
        self.view = View(self)

    def launch_bwt_construction(self, user_input, label_to_set):
        """
         Link sequence_to_bwt function from Model and the interface

            Arguments:
                self: Current Controller object
                user_input: tkinter entry from the interface
                label_to_set: tkinter label from th interface
        """
        # Handling Exception
        try:
            # Retrieve the user sequence from entry
            sequence = user_input.get()
            # Execute sequence_to_bwt Model function with the sequence in argument
            # and assign it in variable
            bwt_sequence, bwt_matrix = sequence_to_bwt(sequence)
            # If program in pedagogic mode
            if self.view.current_mode == "Pedagogic":
                # Retrieve Text label for display information from
                # view.pedagogic_impression attribute
                pedagogic_text_label = self.view.pedagogic_impression
                # Insert in pedagogic_text_label two lines, end argument specify we want
                # to insert the string at the end of existent text
                pedagogic_text_label.insert('end', "Start Burrows Wheeler transformation\n\n")
                pedagogic_text_label.insert('end', "Matrix construction\n")
                # Execute pedagogic_display View function in argument the return of
                # sequence_to_bwt function dictionary with information to display,
                # boolean for know if we need a counter
                self.view.pedagogic_display(bwt_matrix, {'Final matrix:': bwt_matrix,
                                                         'Transformed sequence:': f"{bwt_sequence} Corresponding "
                                                                                  f"to last character of all "
                                                                                  f"elements in matrix",
                                                         'Program': 'Burrows Wheeler transformation'}, True)

            # Set the text of the tkinter label with string containing the
            # return of the sequence_to_bwt Model function
            label_to_set["text"] = f"Transformed sequence: {bwt_sequence}"

            # Create a file path by executing file_save Model function from bwt script
            # Take in argument the name of the file and the sequence to save
            file_path = file_save("Transformed_BWT_sequence", bwt_sequence)
            # Execute file_handler_2_history Controller function for add to history th file save
            # Take in argument file path
            self.file_handler_2_history(file_path)
        # If an exception is intercepted, we recover exception message
        except Exception as error:
            # Execute error_message View function with exception message in argument
            self.view.error_message(error)

    def launch_compression(self, user_input, label_to_set):
        """
        Link compression_huffman function from Model and the interface

            Arguments:
                self: Current Controller object
                user_input: tkinter entry from the interface
                label_to_set: tkinter label from th interface
        """
        # Handling Exception
        try:
            # Retrieve the user sequence from entry
            sequence = user_input.get()
            # Execute compression_huffman function from model with the sequence in argument
            # Initialized compressed_sequence and encoding_tab with the return value of the function
            compressed_sequence, encoding_tab, tree = compression_huffman(sequence)
            # If program in pedagogic mode
            if self.view.current_mode == "Pedagogic":
                # Retrieve Text label for display information from view.pedagogic_impression attribute
                pedagogic_text_label = self.view.pedagogic_impression
                # Insert in pedagogic_text_label line, end argument specify we want
                # to insert the string at the end of existent text
                pedagogic_text_label.insert('end', "Start Huffman compression\n\n")
                # Initialized dictionary with information to display in text
                step_dictionary = {"Decoding keys dictionary:\n": f"{encoding_tab}\n\n",
                                   "Tree construction:\n": [self.display, tree],
                                   "Compressed sequence:\n": f"{compressed_sequence}"}
                # Execute pedagogic_display View function in argument the dictionary with information and
                # dictionary with information to display at the end, boolean for know if
                # we need a counter
                self.view.pedagogic_display(step_dictionary, {'Program': 'Huffman compression'}, counter=False)
            # Set the text of the tinker label with string containing the
            # compressed sequence, the encoding table and the tree from the
            # return of the compression_huffman Model function
            label_to_set["text"] = f"Compressed sequence: {compressed_sequence}\n" \
                                   f"Decoding sequence: {encoding_tab}"
            # Save the compress sequence in a file
            # Execute compression_save from Model with compressed_sequence
            # and encoding_tab in arguments
            compression_save(compressed_sequence, encoding_tab)
            # Execute file_handler_2_history Controller function with file path for add
            # the previous file to the history
            self.file_handler_2_history("Data/compression.txt")
        # If an exception is intercepted, we recover exception message
        except Exception as error:
            # Execute error_message View function with exception message in argument
            self.view.error_message(error)

    def launch_decompression(self, file_path, label_to_set):
        """
        link decompression_huffman function from model and the interface

            Arguments:
                self: Current Controller object
                file_path: list with file information
                label_to_set: tkinter label
        """
        # Handling Exception
        try:
            # Execute recover_information_from_file from Model with the file path in argument
            # and return sequence and decoding table
            sequence, decoding_tab = recover_information_from_file(file_path[1])
            # Execute decompression_huffman function from Model with
            # sequence and decoding_tab in argument and return decompressed sequence
            sequence, binary_code = decompression_huffman(sequence, decoding_tab)
            # If program in pedagogic mode
            if self.view.current_mode == "Pedagogic":
                # Initialized dictionary with information to display in text
                step_dictionary = {"Decoding keys dictionary:\n": f"{decoding_tab}\n\n",
                                   "Binary code:\n": f"{binary_code}\n\n",
                                   "Decompressed sequence:\n": f"{sequence}"}
                # Retrieve Text label for display information from view.pedagogic_impression attribute
                pedagogic_text_label = self.view.pedagogic_impression
                # Insert in pedagogic_text_label line, end argument specify we want
                # to insert the string at the end of existent text
                pedagogic_text_label.insert('end', "Start Huffman decompression\n\n")
                # Execute pedagogic_display View function in argument the dictionary with information and
                # dictionary with information to display at the end, boolean for know if
                # we need a counter
                self.view.pedagogic_display(step_dictionary, {'Program': 'Huffman decompression'}, counter=False)

            # Upper sequence string
            sequence = sequence.upper()
            # Set the text of the tinker label with string containing sequence, from the return of
            # the decompression_huffman Model function
            label_to_set["text"] = f"Decompressed sequence: {sequence}"
            # Execute sequence_draw View function with sequence in argument 7
            # for draw a representation of the sequence
            self.view.sequence_draw(sequence)
            # Execute file_handler_2_history Controller function with file path for add
            # file to the history
            self.file_handler_2_history("Data/decompression.txt")
            # Save the decompressed sequence in a file, Execute decompression_save from
            # Model with sequence in arguments
            decompression_save(sequence)
            # If an exception is intercepted, we recover exception message
        except Exception as error:
            # Execute error_message View function with exception message in argument
            self.view.error_message(error)

    def launch_reverse_bwt(self, user_input, label_to_set):
        """
        link bwt_to_sequence function from Model and the interface

            Arguments:
                self: Current Controller object
                user_input: tkinter entry from the interface
                label_to_set: tkinter label from th interface
        """
        # Handling Exception
        try:
            # Retrieve the user sequence from entry
            sequence = user_input.get()
            # Execute bwt_to_sequence function from model with the sequence in argument
            # Initialized sequence with the return value of the function
            sequence, matrix = bwt_to_sequence(sequence)
            # If program in pedagogic mode
            if self.view.current_mode == "Pedagogic":
                # Retrieve Text label for display information from view.pedagogic_impression attribute
                pedagogic_text_label = self.view.pedagogic_impression
                # Insert in pedagogic_text_label two lines, end argument specify we want
                # to insert the string at the end of existent text
                pedagogic_text_label.insert('end', "Start reverse Burrows Weeler\n\n")
                pedagogic_text_label.insert('end', "Matrix construction:\n")
                # Execute pedagogic_display View function in argument matrix and
                # dictionary with information to display at the end, boolean for know if
                # we need a counter
                self.view.pedagogic_display(matrix, {'Final matrix:': matrix,
                                                     'Sequence reconstructed:': f"{sequence} Corresponding in the "
                                                                                f"matrix to the element with $ in "
                                                                                f"last position",
                                                     'Program': "End reverse Burrows Wheeler"}, True)
            # Set the text of the tkinter label with string containing sequence
            label_to_set["text"] = f"Reconstructed sequence: {sequence}"
            # Execute file_save function with the name of the file and the sequence
            # Save the file in local for the program
            file_path = file_save("Reverse_BWT_sequence", sequence)
            # Execute file_handler_2_history with the path of the file to
            # save the file in the history frame
            self.file_handler_2_history(file_path)
        # If an exception is intercepted, we recover exception message
        except Exception as error:
            # Execute error_message View function with exception message in argument
            self.view.error_message(error)

    def start_view(self):
        """
        Start the GUI

            Argument:
                self: Current Controller object
        """
        # Execute start_interface View function
        self.view.start_interface()

    @staticmethod
    def file_handler(file_path):
        """
        Retrieve file name from file path

            Argument:
                file_path: file path

            Return:
                file_information: list containing file name and file path
        """
        # Split the file path in a list on "/" character
        file_name = file_path.split("/")
        # Take the file name corresponding to the last element of the list
        file_name = file_name[len(file_name) - 1]
        # Create a list containing file name and file path
        file_information = [file_name, file_path]
        return file_information

    def file_handler_2_history(self, file_path):
        """
        Add to the history a file produce by the program

            Arguments:
                self: Current Controller object
                file_path: file path
        """
        # Execute file_handler Controller function for retrieve information of the file
        file_information = self.file_handler(file_path)
        # Execute add_history View function with file_information in argument
        # for add th file to the history
        self.view.add_history(file_information)

    def local_handler_2_frame(self, file_path):
        """
        Add to the history a file from local path

            Arguments:
                self: Current Controller object
                file_path: file path
        """
        # Execute file_handler Controller function for retrieve information of the file
        file_information = self.file_handler(file_path)
        # Set selected_file View attribute with information retrieve
        self.view.selected_file = file_information

    def display(self, node):
        """
        Display for binary tree

            Arguments:
                self: Current Controller object
                node: Node Object
        """
        # From display_aux function affect lines and *_
        lines, *_ = node.display_aux()
        # for each line in lines
        for line in lines:
            # Insert in pedagogic text widget the line with line return at the end
            self.view.pedagogic_impression.insert('end', f"{line}\n")
